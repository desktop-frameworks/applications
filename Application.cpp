/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL::Application is a thin wrapper around QApplication
 * to provide a single instance of an application.
 **/

#include "DFApplication.hpp"
#include "ApplicationImpl.hpp"

#include <csignal>

#include <DFIpcServer.hpp>
#include <DFIpcClient.hpp>

DFL::Application::Application( int& argc, char **argv ) : QApplication( argc, argv ), impl( new DFL::Impl::Application() ) {
    // Handle SIGINT by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGINT );

    // Handle SIGTERM by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGTERM );

    // Handle SIGQUIT by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGQUIT );

    // Handle SIGABRT by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGABRT );

    // Handle SIGSEGV by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGSEGV );

    connect( &DFL::Impl::UnixSignalHandler::instance(), &DFL::Impl::UnixSignalHandler::unixSignalReceived, this, &DFL::Application::unixSignalReceived );
}


DFL::Application::~Application() {
    // No-op
}


void DFL::Application::setApplicationName( QString name ) {
    impl->mAppName = name;
    QApplication::setApplicationName( name );
}


void DFL::Application::setOrganizationName( QString name ) {
    impl->mOrgName = name;
    QApplication::setOrganizationName( name );
}


bool DFL::Application::lockApplication() {
    bool result = impl->lockApplication();

    if ( result && impl->mIpcServer ) {
        // Connect mIpcServer's messageReceived signal to messageFromClient
        connect( impl->mIpcServer.get(), &DFL::IPC::Server::messageReceived, this, &DFL::Application::messageFromClient );
    }

    return result;
}


bool DFL::Application::isRunning() {
    return impl->isRunning();
}


void DFL::Application::interceptSignal( int signum, bool autoHandle ) {
    /** By default we re-route SIGTERM */
    if ( signum == SIGTERM ) {
        /** Do not re-route SIGTERM */
        if ( autoHandle ) {
            DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGTERM );
        }

        /** All others are auto-handled */
        return;
    }

    // There is no provision for re-routing SIGABRT and SIGSEGV.
    // And we are already handling it.
    if ( ( signum == SIGABRT ) || ( signum == SIGSEGV ) ) {
        return;
    }

    // Use a QPointer to track the lifetime of this instance
    QPointer<DFL::Application> self( this );

    // Emit interrupted() or terminate() as suitable.
    DFL::Impl::UnixSignalHandler::instance().interceptSignal(
        signum, [ self, signum ] ( int ) {
            // Emit the terminate() signal if the Application instance is still alive
            if ( self ) {
                QMetaObject::invokeMethod( self, ( signum == SIGQUIT ? "terminate" : "interrupted" ), Qt::QueuedConnection );
            }
        }
    );
}


void DFL::Application::interceptSignal( int signum, std::function<void(int)> func ) {
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( signum, func );
}


bool DFL::Application::messageServer( const QString& message ) {
    if ( impl->initializeClient() ) {
        /**
         * When mIpcClient receives a message, it will be from the server.
         * Connect DFL::IPC::Client::messageReceived to DFL::Application::messageFromServer.
         * This connection
         */
        QObject::connect( impl->mIpcClient.get(), &DFL::IPC::Client::messageReceived, this, &DFL::Application::messageFromServer, Qt::UniqueConnection );

        /** Send the message */
        return impl->messageServer( message );
    }

    /** Be default, we'll assume we failed */
    return false;
}


bool DFL::Application::messageClient( const QString& message, int fd ) {
    return impl->messageClient( message, fd );
}


void DFL::Application::broadcast( const QString& message ) {
    impl->broadcast( message );
}


void DFL::Application::disconnect() {
    /** Disconnect signals and slots */
    QCoreApplication::disconnect();

    impl->disconnect();
}
