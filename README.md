# DFL Applications

This library provides a thin wrapper around QApplication, QGuiApplication and QCoreApplication, to provide
single-instance functionality. Further, with the use of DFL::IPC it also provides a smooth two-way communication
between the first and the subsequent instances.


### Dependencies:
* <tt>Qt Core (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>
* [DFL::IPC](https://gitlab.com/desktop-frameworks/ipc.git)

### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/applications dfl-applications`
- Enter the `dfl-applications` folder
  * `cd dfl-applications`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release -Duse_qt_version=qt5`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Any other feature you request for... :)
