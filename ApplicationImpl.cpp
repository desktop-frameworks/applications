/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL function
 *  getBacktrace collectes the backtrace and demangles the function
 *  names to provide a nice outupt.
 *  This is purely an implementational detail.
 *  This function can be modified/removed at anytime.
 **/


#if __has_include(<execinfo.h>)
    #include <execinfo.h>       // for backtrace
    #include <cxxabi.h>         // for dladdr
    #include <dlfcn.h>          // for __cxa_demangle
#endif

#include <csignal>
#include <cstring>
#include <unistd.h>

#include <sstream>
#include <iostream>
#include <memory>
#include <sys/socket.h>

#include <QDir>
#include <QDebug>
#include <QMutexLocker>
#include <QStandardPaths>

#include <DFIpcServer.hpp>
#include <DFIpcClient.hpp>

#include "ApplicationImpl.hpp"


// A C++ function that will produce a stack trace with demangled function and method names.
void writeBacktrace() {
    #if __has_include(<execinfo.h>)
        void      *callstack[ 128 ];
        const int nMaxFrames = sizeof( callstack ) / sizeof( callstack[ 0 ] );
        char      buf[ 1024 ];
        int       nFrames = backtrace( callstack, nMaxFrames );

        for (int i = 1; i < nFrames; i++) {
            Dl_info info;

            if ( dladdr( callstack[ i ], &info ) ) {
                char *demangled = nullptr;
                int  status;
                demangled = abi::__cxa_demangle( info.dli_sname, nullptr, 0, &status );

                snprintf(
                    buf,
                    sizeof( buf ),
                    "%-3d %*p %s + %zd\n",
                    i,
                    (int)( 2 + sizeof( void * ) * 2 ),
                    callstack[ i ],
                    status == 0 ? demangled : info.dli_sname,
                    (char *)callstack[ i ] - (char *)info.dli_saddr
                );

                // Write to stderr
                write( STDERR_FILENO, buf, strlen( buf ) );

                free( demangled );
            }
            else {
                snprintf(
                    buf,
                    sizeof( buf ),
                    "%-3d %*p\n",
                    i,
                    (int)( 2 + sizeof( void * ) * 2 ),
                    callstack[ i ]
                );

                // Write to stderr
                write( STDERR_FILENO, buf, strlen( buf ) );
            }
        }

        if ( nFrames == nMaxFrames ) {
            const char *truncatedMsg = "  [truncated]\n";
            write( STDERR_FILENO, truncatedMsg, strlen( truncatedMsg ) );
        }

    #else
        const char *noExecInfoMsg = "This library/application was compiled without execinfo.h, cannot provide a backtrace!\n";
        write( STDERR_FILENO, noExecInfoMsg, strlen( noExecInfoMsg ) );
    #endif
}


void defaultSignalHandler( int signum ) {
    std::cerr << "Signal SIG" << sigabbrev_np( signum ) << " received." << std::endl;
    std::cerr << "Backtrace:" << std::endl;
    writeBacktrace();
    std::cerr << std::endl;

    if ( ( signum == SIGABRT ) || ( signum == SIGSEGV ) ) {
        // Do not try to clean up.
        _Exit( signum );
    }

    else {
        // Try a clean exit.
        std::exit( signum );
    }
}


DFL::Impl::Application::~Application() {
    disconnect();
    cleanupLockFile();
}


bool DFL::Impl::Application::lockApplication() {
    if ( mAppName.isEmpty() || mOrgName.isEmpty() ) {
        qWarning() << "Application and Organization names must be set before locking.";
        return false;
    }

    if ( isRunning() ) {
        return false;
    }

    // Clean up stale socket files
    cleanupSocketFile();

    // Start the IPC server
    QString socketPath = getSocketPath() + ".socket";

    /** Create the server instance */
    mIpcServer = std::make_unique<DFL::IPC::Server>( socketPath, nullptr );

    /** Failed to start the server */
    if ( mIpcServer->startServer() == false ) {
        return false;
    }

    // Create a lock file to track the running instance
    createLockFile();

    return true;
}


bool DFL::Impl::Application::isRunning() {
    QString socketPath = getSocketPath() + ".socket";

    return DFL::IPC::Client::checkConnection( socketPath );
}


bool DFL::Impl::Application::initializeClient() {
    /** All set to go */
    if ( mIpcClient && mIpcClient->isConnected() ) {
        return true;
    }

    QString socketPath = getSocketPath();

    /** Create the @mIpcClient instance */
    if ( !mIpcClient ) {
        mIpcClient = std::make_unique<DFL::IPC::Client>( socketPath + ".socket", nullptr );
    }

    /** Try to connect to the server */
    if ( !mIpcClient->connectToServer() ) {
        qWarning() << "Failed to connect to IPC server at" << socketPath;

        mIpcClient.reset();

        return false;
    }

    /** Wait a maximum of 50 ms to get registered, since the process is nearly instantaneous */
    return mIpcClient->waitForRegistered( 50 );
}


bool DFL::Impl::Application::messageServer( const QString& message ) {
    if ( initializeClient() ) {
        return mIpcClient->sendMessage( message );
    }

    return false;
}


bool DFL::Impl::Application::messageClient( const QString& message, int fd ) {
    if ( mIpcServer ) {
        return mIpcServer->reply( fd, message );
    }

    return false;
}


void DFL::Impl::Application::broadcast( const QString& message ) {
    mIpcServer->broadcast( message );
}


void DFL::Impl::Application::disconnect() {
    if ( mIpcServer ) {
        mIpcServer->shutdown();
        mIpcServer.reset();
    }

    if ( mIpcClient ) {
        mIpcClient->disconnectFromServer();
        mIpcClient.reset();
    }

    // Clean up the lock file
    cleanupLockFile();
}


QString DFL::Impl::Application::getSocketPath() const {
    if ( mAppName.isEmpty() ) {
        qCritical() << "Application name is not set. Cannot lock application.";
        return QString();
    }

    if ( mOrgName.isEmpty() ) {
        qCritical() << "Organization name is not set. Cannot lock application.";
        return QString();
    }

    /** Create the socket path */
    QString sockPath( "%1/%2" );
    QString sockDir( "%1/%2/%3" );

    sockDir = sockDir.arg( QStandardPaths::writableLocation( QStandardPaths::RuntimeLocation ) )
                 .arg( QString( mOrgName ).replace( " ", "" ) )
                 .arg( QString( qgetenv( "XDG_SESSION_ID" ) ) );

    /** Create this folder */
    if ( QDir( "/" ).mkpath( sockDir ) == false ) {
        qCritical() << "Failed to created socket path:" << sockDir;
        return QString();
    }

    sockPath = sockPath.arg( sockDir ).arg( QString( mAppName ).replace( " ", "" ) );

    return sockPath;
}


void DFL::Impl::Application::createLockFile() {
    QString lockFilePath = getSocketPath() + ".lock";
    QFile   lockFile( lockFilePath );

    if ( lockFile.open( QIODevice::WriteOnly ) ) {
        lockFile.write( QString::number( QCoreApplication::applicationPid() ).toUtf8() );
        lockFile.close();
    }
}


void DFL::Impl::Application::cleanupLockFile() {
    QString lockFilePath = getSocketPath() + ".lock";

    QFile::remove( lockFilePath );
}


void DFL::Impl::Application::cleanupSocketFile() {
    QString socketPath = getSocketPath() + ".socket";

    QFile::remove( socketPath );
}


/**
 * DFL::Impl::UnixSignalHandler
 **/
DFL::Impl::UnixSignalHandler& DFL::Impl::UnixSignalHandler::instance() {
    static UnixSignalHandler handler;

    return handler;
}


DFL::Impl::UnixSignalHandler::UnixSignalHandler( QObject *parent ) : QObject( parent ) {
}


DFL::Impl::UnixSignalHandler::~UnixSignalHandler() {
}


void DFL::Impl::UnixSignalHandler::interceptSignal( int signum, std::function<void(int)> func ) {
    struct sigaction sa;

    sa.sa_handler = UnixSignalHandler::staticSignalHandler;
    sigemptyset( &sa.sa_mask );
    sa.sa_flags = 0;

    if ( ::sigaction( signum, &sa, nullptr ) == -1 ) {
        std::cerr << "Failed to register signal handler for signal " << signum << std::endl;
        return;
    }

    // Store the callback function for this signal
    signalToCallbackMap[ signum ] = func;
}


void DFL::Impl::UnixSignalHandler::staticSignalHandler( int signum ) {
    // Use QMetaObject::invokeMethod to emit the signal from the main thread
    QMetaObject::invokeMethod(
        &instance(),                     // QObject instance (UnixSignalHandler)
        "unixSignalReceived",            // Signal name
        Qt::QueuedConnection,            // Connection type (queued for thread safety)
        Q_ARG( int, signum )             // Signal argument
    );

    // Check if the signal is safe (SIGINT, SIGTERM, SIGQUIT)
    static const QList<int> safeSignals = {
        SIGINT,
        SIGTERM,
        SIGQUIT,
    };

    bool isSafeSignal = safeSignals.contains( signum );

    // Handle safe signals
    if ( isSafeSignal ) {
        QMutexLocker locker( &instance().mapMutex ); // Lock the mutex for thread safety

        auto& signalsReceivedList = instance().signalsReceivedList;

        // Check if any safe signal has been received before
        bool hasReceivedSafeSignal = signalsReceivedList.contains( SIGINT ) ||
                                     signalsReceivedList.contains( SIGTERM ) ||
                                     signalsReceivedList.contains( SIGQUIT );

        // If this is the first safe signal and the default handler is being used
        if ( !hasReceivedSafeSignal && ( instance().signalToCallbackMap[ signum ] == nullptr ) ) {
            const char *msg1 = "Waiting for the program to handle SIG";
            const char *msg2 = "\nPress SIG";
            const char *msg3 = " to quit immediately.\n";

            write( STDERR_FILENO, msg1,                   strlen( msg1 ) );
            write( STDERR_FILENO, sigabbrev_np( signum ), strlen( sigabbrev_np( signum ) ) );
            write( STDERR_FILENO, msg2,                   strlen( msg2 ) );
            write( STDERR_FILENO, sigabbrev_np( signum ), strlen( sigabbrev_np( signum ) ) );
            write( STDERR_FILENO, msg3,                   strlen( msg3 ) );

            // Record that this safe signal has been received
            signalsReceivedList << signum;
            return;
        }
    }

    // Call the registered callback, if any
    auto& callbackMap = instance().signalToCallbackMap;
    auto  it          = callbackMap.find( signum );

    if ( it != callbackMap.end() ) {
        // Retrieve the callback
        auto callback = it.value();

        if ( callback ) {
            // Invoke the callback
            callback( signum );
        }
        else {
            defaultSignalHandler( signum );
        }
    }
}
