/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL::GuiApplication is a thin wrapper around QGuiApplication
 * to provide a single instance of an application.
 **/

#include "DFGuiApplication.hpp"
#include "ApplicationImpl.hpp"

#include <csignal>

#include <DFIpcServer.hpp>
#include <DFIpcClient.hpp>

DFL::GuiApplication::GuiApplication( int& argc, char **argv ) : QGuiApplication( argc, argv ), impl( new DFL::Impl::Application() ) {
    // Handle SIGINT by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGINT );

    // Handle SIGTERM by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGTERM );

    // Handle SIGQUIT by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGQUIT );

    // Handle SIGABRT by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGABRT );

    // Handle SIGSEGV by default
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGSEGV );

    connect( &DFL::Impl::UnixSignalHandler::instance(), &DFL::Impl::UnixSignalHandler::unixSignalReceived, this, &DFL::GuiApplication::unixSignalReceived );
}


DFL::GuiApplication::~GuiApplication() {
}


void DFL::GuiApplication::setApplicationName( QString name ) {
    impl->mAppName = name;
    QGuiApplication::setApplicationName( name );
}


void DFL::GuiApplication::setOrganizationName( QString name ) {
    impl->mOrgName = name;
    QGuiApplication::setOrganizationName( name );
}


bool DFL::GuiApplication::lockApplication() {
    bool result = impl->lockApplication();

    if ( result && impl->mIpcServer ) {
        // Connect mIpcServer's messageReceived signal to messageFromClient
        connect( impl->mIpcServer.get(), &DFL::IPC::Server::messageReceived, this, &DFL::GuiApplication::messageFromClient );
    }

    return result;
}


bool DFL::GuiApplication::isRunning() {
    return impl->isRunning();
}


void DFL::GuiApplication::interceptSignal( int signum, bool autoHandle ) {
    /** By default we re-route SIGTERM */
    if ( signum == SIGTERM ) {
        /** Do not re-route SIGTERM */
        if ( autoHandle ) {
            DFL::Impl::UnixSignalHandler::instance().interceptSignal( SIGTERM );
        }

        /** All others are auto-handled */
        return;
    }

    // There is no provision for re-routing SIGABRT and SIGSEGV.
    // And we are already handling it.
    if ( ( signum == SIGABRT ) || ( signum == SIGSEGV ) ) {
        return;
    }

    // Use a QPointer to track the lifetime of this instance
    QPointer<DFL::GuiApplication> self( this );

    // Emit interrupted() or terminate() as suitable.
    DFL::Impl::UnixSignalHandler::instance().interceptSignal(
        signum, [ self, signum ] ( int ) {
            // Emit the terminate() signal if the Application instance is still alive
            if ( self ) {
                QMetaObject::invokeMethod( self, ( signum == SIGQUIT ? "terminate" : "interrupted" ), Qt::QueuedConnection );
            }
        }
    );
}


void DFL::GuiApplication::interceptSignal( int signum, std::function<void(int)> func ) {
    DFL::Impl::UnixSignalHandler::instance().interceptSignal( signum, func );
}


bool DFL::GuiApplication::messageServer( const QString& message ) {
    if ( impl->initializeClient() ) {
        /**
         * When mIpcClient receives a message, it will be from the server.
         * Connect DFL::IPC::Client::messageReceived to DFL::Application::messageFromServer.
         * This connection
         */
        QObject::connect( impl->mIpcClient.get(), &DFL::IPC::Client::messageReceived, this, &DFL::GuiApplication::messageFromServer, Qt::UniqueConnection );

        /** Send the message */
        return impl->messageServer( message );
    }

    /** Be default, we'll assume we failed */
    return false;
}


bool DFL::GuiApplication::messageClient( const QString& message, int fd ) {
    return impl->messageClient( message, fd );
}


void DFL::GuiApplication::broadcast( const QString& message ) {
    impl->broadcast( message );
}


void DFL::GuiApplication::disconnect() {
    /** Disconnect signals and slots */
    QGuiApplication::disconnect();

    impl->disconnect();
}
