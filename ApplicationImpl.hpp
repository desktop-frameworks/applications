/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * IMPL function
 *  getBacktrace collectes the backtrace and demangles the function
 *  names to provide a nice outupt.
 *  This is purely an implementational detail.
 *  This function can be modified/removed at anytime.
 **/

#pragma once

#include <QHash>
#include <QString>
#include <QObject>
#include <QMutex>

#include <functional>

namespace DFL {
    class CoreApplication;
    class GuiApplication;
    class Application;

    namespace IPC {
        class Server;
        class Client;
    }

    namespace Impl {
        class Application;
        class UnixSignalHandler;
    }
}

class QLockFile;
class QSocketNotifier;

/**
 * Pretty-format the backtrace.
 * This function demangles the namespaces and prettifies
 * the backtrace.
 */
void writeBacktrace();

/**
 * This is the default signal intercept handler.
 * By default, this will print the backtrace,
 * and call either std::exit(...) or _Exit(...).
 */
void defaultSignalHandler( int );

class DFL::Impl::Application {
    public:
        /** Constructor */
        Application() = default;

        ~Application();

        /**
         * Lock this instance of the application.
         * When this function is called, it will first search if another
         * instance of this app is running. If found, it will return false.
         * Otherwise, it will start the IPC server and return true.
         * NOTE: Application and Organization name need to be set before
         * calling this function. Otherwise, locking will not be performed.
         */
        bool lockApplication();

        /**
         * Check if another instance of this application is running.
         */
        bool isRunning();

        /**
         * Initialize the @mIpcClient instance and connect to the server
         */
        bool initializeClient();

        /**
         * Send a message to the server.
         */
        bool messageServer( const QString& message );

        /**
         * Send a message to a specific client.
         */
        bool messageClient( const QString& message, int fd );

        /**
         * Broadcast a message to all connected clients.
         */
        void broadcast( const QString& message );

        /**
         * Disconnect from the server and clean up IPC resources.
         */
        void disconnect();

    protected:

        /**
         * Get the socket path.
         * This function generates the socket path from the application name,
         * organization name, and XDG runtime directory.
         */
        QString getSocketPath() const;

        /**
         * Daughter classes need to be able to access these when calling
         * the respective setters.
         */
        QString mAppName;
        QString mOrgName;

        /**
         * Daughter classes will need to access these to create signal-clot
         * connections.
         */
        std::unique_ptr<DFL::IPC::Server> mIpcServer;
        std::unique_ptr<DFL::IPC::Client> mIpcClient;

    private:

        /**
         * Create a lock file to track the running instance.
         */
        void createLockFile();

        /**
         * Clean up the lock file.
         */
        void cleanupLockFile();

        /**
         * Clean up stale socket files.
         */
        void cleanupSocketFile();

        friend class DFL::CoreApplication;
        friend class DFL::GuiApplication;
        friend class DFL::Application;
};


class DFL::Impl::UnixSignalHandler : public QObject {
    Q_OBJECT;

    public:

        /**
         * Singleton instance of the UnixSignalHandler.
         */
        static UnixSignalHandler& instance();

        /**
         * Intercept a Unix signal and optionally provide a callback function.
         * If the callback is nullptr, the signal will be handled internally.
         * The callback receives the signal number (signum) as a parameter.
         */
        void interceptSignal( int signum, std::function<void(int)> func = nullptr );

        /**
         * Emitted when a Unix signal is received.
         */
        Q_SIGNAL void unixSignalReceived( int signum );

    private:

        /**
         * Constructor - Allocate the resources, and init signal-slot connections
         * This is private so that users cannot create new instances.
         */
        UnixSignalHandler( QObject *parent = nullptr );

        /**
         * Destructor - Clean up the resources.
         * This is private so that users cannot delete the existing instance
         */
        ~UnixSignalHandler();

        /**
         * Static signal handler function to write to the pipe.
         */
        static void staticSignalHandler( int signum );

        // Map of signals to callbacks
        QHash<int, std::function<void(int)> > signalToCallbackMap;

        /**
         * Keep track of the signals recieved.
         * Default SIGNAL handling behaviour:
         * If any of SIGINT, SIGTERM, SIGQUIT were received previously,
         * print the trackaback and quit the application. Otherwise,
         * If it's the first time, emit unixSignalReceived, and wait.
         */
        QList<int> signalsReceivedList;

        // Mutex to protect signalReceivedMap
        QMutex mapMutex;
};
