project(
	'DFL Application',
	'cpp',
	version: '0.2.0',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_project_arguments( '-DPROJECT_VERSION=' + meson.project_version(), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

dl = meson.get_compiler( 'cpp' ).find_library( 'dl' )

if get_option('use_qt_version') == 'qt5'
	Qt = import( 'qt5' )
	QtCore    = dependency( 'qt5', modules: [ 'Core' ] )
    QtGui     = dependency( 'qt5', modules: [ 'Gui' ] )
    QtWidgets = dependency( 'qt5', modules: [ 'Widgets' ] )

	IPC       = dependency( 'df5ipc' )

	corename    = 'df5coreapplication'
	guiname     = 'df5guiapplication'
	widgetname  = 'df5application'

	subdirname = 'DFL/DF5'

elif get_option('use_qt_version') == 'qt6'
	Qt = import( 'qt6' )
	QtCore    = dependency( 'qt6', modules: [ 'Core' ] )
    QtGui     = dependency( 'qt6', modules: [ 'Gui' ] )
    QtWidgets = dependency( 'qt6', modules: [ 'Widgets' ] )

	IPC       = dependency( 'df6ipc' )

	corename    = 'df6coreapplication'
	guiname     = 'df6guiapplication'
	widgetname  = 'df6application'

	subdirname = 'DFL/DF6'

endif

CoreDeps   = [ QtCore, IPC, dl ]
GuiDeps    = [ CoreDeps, QtGui ]
WidgetDeps = [ GuiDeps, QtWidgets ]

# backtrace() is in a separate library on FreeBSD and Linux with musl
backtrace = meson.get_compiler( 'cpp' ).find_library( 'execinfo', required: false )
if ( not backtrace.found() )
	if ( meson.get_compiler( 'cpp' ).has_header( 'execinfo.h' ) )
		message( 'Compiling with backtrace() support.' )
	else
		message( 'backtrace() support unavailable.' )
	endif
endif

AIHeaders = [
	'ApplicationImpl.hpp',
]

AISources = [
	'ApplicationImpl.cpp',
]

AIMocs = Qt.compile_moc(
 	headers : [ AIHeaders ],
	dependencies: QtCore
)

applimpl = static_library(
    'applimpl', [ AISources, AIMocs ],
    dependencies: [QtCore, IPC],
    install: false,
)


CoreHeaders = [
	'DFCoreApplication.hpp',
]

CoreSources = [
	'CoreApplication.cpp',
]

CoreMocs = Qt.compile_moc(
 	headers : [ CoreHeaders ],
	dependencies: CoreDeps
)

coreapp = shared_library(
    corename, [ CoreSources, CoreMocs ],
	version: meson.project_version(),
    dependencies: CoreDeps,
	link_with: applimpl,
    install: true,
    install_dir: get_option( 'libdir' )
)

GuiHeaders = [
	'DFGuiApplication.hpp',
]

GuiSources = [
	'GuiApplication.cpp',
]

GuiMocs = Qt.compile_moc(
 	headers : GuiHeaders,
	dependencies: GuiDeps
)

guiapp = shared_library(
    guiname, [ GuiSources, GuiMocs ],
	version: meson.project_version(),
    dependencies: GuiDeps,
	link_with: applimpl,
    install: true,
    install_dir: get_option( 'libdir' )
)

WidgetHeaders = [
	'DFApplication.hpp',
]

WidgetSources = [
	'Application.cpp',
]

WidgetMocs = Qt.compile_moc(
 	headers : WidgetHeaders,
	dependencies: WidgetDeps
)

widgetapp = shared_library(
    widgetname, [ WidgetSources, WidgetMocs ],
	version: meson.project_version(),
    dependencies: WidgetDeps,
	link_with: applimpl,
    install: true,
    install_dir: get_option( 'libdir' )
)

install_headers( [ 'DFCoreApplication.hpp', 'DFGuiApplication.hpp', 'DFApplication.hpp' ], subdir : subdirname )

## PkgConfig Section
pkgconfig = import( 'pkgconfig' )
pkgconfig.generate(
	coreapp,
	name: corename,
	subdirs: subdirname,
 	version: meson.project_version(),
	filebase: corename,
	description: 'Single Instance CoreApplication class',
	requires: IPC
)

pkgconfig.generate(
	guiapp,
	name: guiname,
	subdirs: subdirname,
 	version: meson.project_version(),
	filebase: guiname,
	description: 'Single Instance GuiApplication class',
	requires: IPC
)

pkgconfig.generate(
	widgetapp,
	name: widgetname,
	subdirs: subdirname,
 	version: meson.project_version(),
	filebase: widgetname,
	description: 'Single Instance Application class',
	requires: IPC
)
